module.exports = function initialize($){
/****************************************************************************/

var path = require('path'),
    fs = require('fs');

var BASEPATH = path.resolve(__dirname, '..');
$.config = {};

// -------- read config files

var confPath = path.resolve(BASEPATH, 'config.json'),
    conf = JSON.parse(fs.readFileSync(confPath));

$.config.gpg = {}
$.config.gpg.bin = conf.gpg.bin || '/usr/bin/gpg';
$.config.gpg.homedir = path.resolve(
    BASEPATH,
    conf.gpg.homedir || './.storage'
);

console.log($)

/****************************************************************************/
}

module.exports({})
