Python wrappers of GPG
======================

The most useful commands of GPG are listed here. The author has decided to
wrap the command-line driven GPG command here, since no other libraries
satisfies his needs and flexibilities. Python is also more easy in doing
interfaces to GPG(as compared to NodeJS). In future this may be divided into
a new project.

For each function, there has been a program under the similar name in GPG
command line. They are done with `argparse` module with helping infos.
