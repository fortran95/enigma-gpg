#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Attempt to decrypt given data.

This program will provide 2 functions:
1) to show which key has to be used to decrypt. This makes it possible for
   other programs let the user input proper passphrase.
2) try to decrypt the given data using one or a series of keys.
"""

import sys
import subprocess
import argparse

parser = argparse.ArgumentParser(\
    description="""Decrypt STDIN input in armored(ASCII) ciphertext.
    The actual decryption must be done with --with-key and --passphrase
    options. If none of them are specified, the script will return a list of
    possible keys used for decryption. Both --with-key and --passphrase can be
    used multiple times, meaning that a series of keys are supplied. In such
    case, their orders are critical for matching the key with passphrase.
    """
)

parser.add_argument(
    "--homedir",
    action="store",
    type=str,
    help="Homedir for GPG."
)

parser.add_argument(
    "--with-key",
    metavar="USER_ID",
    action="store",
    nargs="+",
    type=str,
    help="""
    Decrypt using given key. This must be used together with option
    --passphrase.
    """
)

parser.add_argument(
    "--passphrase",
    metavar="PASSPHRASE",
    action="store",
    nargs="+",
    type=str,
    help="""
    The passphrase for the given key(in order).
    """
)

args = parser.parse_args()
exit()
##############################################################################

GPGCMD = ['gpg']

if args.homedir != None:
    GPGCMD += ['--homedir', args.homedir]

GPGCMD += ['--armor', '--encrypt']

for each in args.recipients:
    if args.hidden_recipient:
        GPGCMD += ['-R', each]
    else:
        GPGCMD += ['-r', each]

data = sys.stdin.read()

gpg = subprocess.Popen(\
    GPGCMD, 
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE
)
gpgout, gpgerr = gpg.communicate(input=data)

if '' != gpgerr.strip():
    sys.stderr.write(gpgerr)
    exit(1)

sys.stdout.write(gpgout)

