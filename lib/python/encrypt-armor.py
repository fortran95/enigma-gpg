#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Generate armored ciphertext for given recipients.

Returns 1 if error were raised in encrypting progress.
"""

import sys
import subprocess
import argparse

parser = argparse.ArgumentParser(\
    description="Encrypt STDIN input in armored(ASCII) ciphertext with "\
    "given recipients."
)

parser.add_argument(
    "--homedir",
    action="store",
    type=str,
    help="Homedir for GPG."
)

parser.add_argument(
    "--hidden-recipient",
    action="store_true",
    default=False,
    help="If given, no recipient info will be specified in the final "+\
         "ciphertext."
)

parser.add_argument(
    "recipients",
    metavar="<RECIPIENT_KEYID>",
    action="store",
    nargs="+",
    type=str,
    help="KeyID of recipients."
)

args = parser.parse_args()

##############################################################################

GPGCMD = ['gpg']

if args.homedir != None:
    GPGCMD += ['--homedir', args.homedir]

GPGCMD += ['--armor', '--encrypt']

for each in args.recipients:
    if args.hidden_recipient:
        GPGCMD += ['-R', each]
    else:
        GPGCMD += ['-r', each]

data = sys.stdin.read()

gpg = subprocess.Popen(\
    GPGCMD, 
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE
)
gpgout, gpgerr = gpg.communicate(input=data)

if '' != gpgerr.strip():
    sys.stderr.write(gpgerr)
    exit(1)

sys.stdout.write(gpgout)
