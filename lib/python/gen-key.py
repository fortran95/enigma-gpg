#!/usr/bin/env python
# -*- coding: utf-8 -*-

SUPPORTED_ALGORITHMS = [
    "ELG-DSA",
    "RSA-RSA",
    "XXX-RSA",
    "XXX-DSA",
]

##############################################################################

import subprocess
import sys
import os
import argparse

parser = argparse.ArgumentParser(\
    description="Generate a GPG key in non-interactive mode."
)

parser.add_argument(
    "--homedir",
    action="store",
    type=str,
    help="Homedir for GPG."
)
parser.add_argument(
    "--bits",
    action="store",
    type=int,
    help="Bits for the new key. Default is 3072.", 
    choices=[1024, 2048, 3072, 4096],
    default=3072
)
parser.add_argument(
    "--keytype",
    action="store",
    type=str,
    help="Select the algorithms for the key. The combination is AAA-BBB, " +\
         "where AAA is the encryption method and BBB is the sign method. " +\
         "If AAA is XXX, the key will be only used for signing. Default " + \
         "is RSA-RSA.",
    choices=SUPPORTED_ALGORITHMS,
    default="RSA-RSA"
)
parser.add_argument(
    "--expiration",
    action="store",
    type=int,
    help="Use this option if you want to specify a date for expiration." +\
         "The units are in days. Default is no expiration",
    default=0
)

parser.add_argument(
    "name",
    action="store",
    metavar="NAME",
    nargs=1,
    help="Real name of the key owner."
)
parser.add_argument(
    "passphrase",
    action="store",
    metavar="PASSPHRASE",
    nargs=1,
    help="Specify a passphrase for this key."
)

args = parser.parse_args()

##############################################################################

BATCHCMD = []

# -------- determine key type and key length

keytypeMapping = {
    "ELG-DSA": ("ELG-E", "DSA"),
    "RSA-RSA": ("RSA", "RSA"),
    "XXX-RSA": (None, "RSA"),
    "XXX-DSA": (None, "DSA"),
}

varSubkeyType, varMainkeyType = keytypeMapping[args.keytype]

BATCHCMD.append("Key-Type: %s" % varMainkeyType)
BATCHCMD.append("Key-Length: %d" % args.bits)

if None != varSubkeyType:
    BATCHCMD.append("Subkey-Type: %s" % varSubkeyType)
    BATCHCMD.append("Subkey-Length: %d" % args.bits)

# -------- add passphrase

BATCHCMD.append("Passphrase: %s" % args.passphrase[0])

# -------- add name

BATCHCMD.append("Name-Real: %s" % args.name[0])

# -------- add expire date

if 0 != args.expiration:
    BATCHCMD.append("Expire-Date: %dd" % args.expiration)

##############################################################################

BATCHCMD = "\n".join(BATCHCMD)

if args.homedir:
    HOMEDIR = ' --homedir %s' % args.homedir
else:
    HOMEDIR = ''
GPGCMD = "gpg%s --batch --no-tty --gen-key <<EOF\n%s\nEOF" % (\
    HOMEDIR, BATCHCMD
)

subprocess.call(GPGCMD, shell=True)
