#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Parse the GPG listed public or private keys.

See <http://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob_plain;f=doc
/DETAILS> for more details on its format.
"""

import argparse
import json
import subprocess


parser = argparse.ArgumentParser(\
    description="List stored GPG keys with better machine friendly format."
)

parser.add_argument(
    "--homedir",
    action="store",
    type=str,
    help="Homedir for GPG."
)

parser.add_argument(
    "--secret",
    action="store_true",
    default=False,
    help="List secret keys using: gpg --list-secret-keys."
)

args = parser.parse_args()

##############################################################################

verb = '--list-keys'
if args.secret:
    verb = '--list-secret-keys'

GPGCMD = [\
    '--batch',
    '--with-colons',
    '--fixed-list-mode', 
    '--fingerprint',
    verb
]
if args.homedir != None:
    GPGCMD = ['--homedir', args,homedir] + GPGCMD

GPGCMD = ['gpg'] + GPGCMD
keylist = subprocess.check_output(GPGCMD)

keylist = keylist.split('\n')
recording = False
record = None
records = []
for l in keylist:
    fields = l.strip().split(':')

    if len(fields) < 11:
        continue
    fields += [''] * (16 - len(fields))
    typeOfRecord, validity, keyLen, pubkeyAlgo, keyID,\
    creationDate, expireDate, certUID, ownerTrust, userID,\
    signatureClass, keyCapabilities = fields[:12]

    encounteredNewKey = typeOfRecord in ['sec', 'pub'] 
    if encounteredNewKey:
        if record:
            records.append(record)
        record = {"keys":[], "user": {}}

    if typeOfRecord in ['pub', 'sec', 'sub', 'ssb']:
        if not record.has_key("keys"):
            record["keys"] = []

        record["keys"].append({
            "ID": keyID,
            "algorithm": pubkeyAlgo, 
            "bits": int(keyLen),
            "secret": (typeOfRecord in ['sec', 'ssb']),
            "main": (typeOfRecord in ['pub', 'sec']),
        })
    if typeOfRecord == 'uid':
        record["user"]["ID"] = userID
    if typeOfRecord == 'fpr':
        record["user"]["fingerprint"] = userID
if record:
    records.append(record)

print json.dumps(records)
